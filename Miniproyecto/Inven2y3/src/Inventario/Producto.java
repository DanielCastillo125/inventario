package Inventario;

public class Producto {
	
    private int id;
    private String nombre;
    private int cantidad;
    private Proveedor proveedor;
    private double precioCompra;
    private double precioVenta;

	public Producto(int id, String nombre, Proveedor proveedor, double precioCompra, double precioVenta) {
		this.id = id;
		this.nombre = nombre;
		this.proveedor = proveedor;
		this.precioCompra = precioCompra;
		this.precioVenta = precioVenta;
		cantidad = 0;
	}
	public Producto(int id, String nombre, Proveedor proveedor, double precioCompra, double precioVenta, int cantidad) {
		this.id = id;
		this.nombre = nombre;
		this.proveedor = proveedor;
		this.precioCompra = precioCompra;
		this.precioVenta = precioVenta;
		this.cantidad = cantidad;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public double getPrecioCompra() {
		return precioCompra;
	}

	public void setPrecioCompra(double precioCompra) {
		this.precioCompra = precioCompra;
	}

	public double getPrecioVenta() {
		return precioVenta;
	}

	public void setPrecioVenta(double precioVenta) {
		this.precioVenta = precioVenta;
	}
    
}
