package Inventario;
import java.sql.*;
public class Compra {
	
	private int id;
	private Producto producto;
	private Proveedor proveedor;
	private int cantidad;
	private String fecha;
	private double precioFinal;
	
	public Compra(int id, Producto producto, Proveedor proveedor, int cantidad, String fecha, double precioFinal) {
		this.id = id;
		this.producto = producto;
		this.proveedor = proveedor;
		this.cantidad = cantidad;
		this.fecha = fecha;
		this.precioFinal = precioFinal;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Proveedor getProveedor() {
		return proveedor;
	}

	public void setProveedor(Proveedor proveedor) {
		this.proveedor = proveedor;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public double getPrecioFinal() {
		return precioFinal;
	}

	public void setPrecioFinal(double precioFinal) {
		this.precioFinal = precioFinal;
	}
	

	
	
}
