package Inventario;

public class Caja {
	private int id;
	private double valorIngreso;
	private double valorActual;
	
	public Caja(int id, double valorIngreso, double valorActual) {
		this.id = id;
		this.valorIngreso = valorIngreso;
		this.valorActual = valorActual;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getValorIngreso() {
		return valorIngreso;
	}

	public void setValorIngreso(double valorIngreso) {
		this.valorIngreso = valorIngreso;
	}

	public double getValorActual() {
		return valorActual;
	}

	public void setValorActual(double valorActual) {
		this.valorActual = valorActual;
	}
	
	
}
