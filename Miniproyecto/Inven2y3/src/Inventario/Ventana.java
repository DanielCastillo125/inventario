package Inventario;

import java.awt.*;
import java.sql.*;
import java.sql.Date;

import javax.swing.*;
import java.util.*;

public class Ventana{
	private JFrame ventanaIni;
	private JFrame venFondo;
	private VentanaModal ventanaAgregar;
	private JButton botonCon;
	private JButton botonCom;
	private JButton botonVen;
	private JButton botonAgregar;
	private JButton botonEditar;
    private Container contenedor;
    
    private JPanel esquema;
    private JPanel botones;
    private JPanel botonesSec;
    private JPanel botonesTer;
    private JComboBox select;
    
    private ArrayList<Producto> productos = new ArrayList<Producto>();
    private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    private ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
    private ArrayList<Venta> ventas = new ArrayList<Venta>();
    private ArrayList<Compra> compras = new ArrayList<Compra>();
    private ArrayList<Caja> capital = new ArrayList<Caja>();
    private Caja ultima;
    
	private Connection miConexion;
    
    
	public Ventana() {
		
		try {
			
			miConexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventario", "root", "");
		
		}catch (Exception e) {
			System.out.println("no conecta");
			e.printStackTrace();
		}
		ventanaIni = new JFrame();
        
        contenedor = ventanaIni.getContentPane();
        contenedor.setLayout(new BorderLayout(5,5));
        
        ventanaIni.setDefaultCloseOperation(3);
        ventanaIni.setTitle("Gestor de Inventario");
        ventanaIni.setSize(400,300);
        ventanaIni.setLocationRelativeTo(null);
        ventanaIni.setResizable(false);

        JMenuBar barraDeMenu = new JMenuBar();
        ventanaIni.setJMenuBar(barraDeMenu);
        
        JMenu fondos = new JMenu("Fondos");
        barraDeMenu.add(fondos);
        
        JMenuItem agreFondos = new JMenuItem("Agregar Fondos");
        agreFondos.addActionListener( e -> venAgregarFondos());
        fondos.add(agreFondos);
        
        JMenuItem verFondos = new JMenuItem("Ver Fondos");
        verFondos.addActionListener( e -> venFondos());
        fondos.add(verFondos);
        
        JMenu corregir = new JMenu("Corregir Errores");
        barraDeMenu.add(corregir);
        
        JMenuItem corVenta = new JMenuItem("Anular Venta");
        corVenta.addActionListener( e -> venAnularVenta());
        corregir.add(corVenta);
        
        JMenuItem corCompra = new JMenuItem("Anular Compra");
        corCompra.addActionListener( e -> venAnularCompra());
        corregir.add(corCompra);
        
        
        botones = new JPanel();
        botones.setLayout(new GridLayout());
        
        botonCon = new JButton("CONSULTAR");
        botonCon.addActionListener( e -> ventanaConsulta());
        botonCon.setEnabled(false);
        botones.add(botonCon);
       
        botonCom = new JButton("COMPRAR");
        botonCom.addActionListener( e -> ventanaCompra());
        botones.add(botonCom);
        

        botonVen = new JButton("VENDER");
        botonVen.addActionListener( e -> ventanaVenta());
        botones.add(botonVen);
        
        contenedor.setBackground(Color.WHITE);
        
        
        botonesSec = new JPanel();
        botonesSec.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
        
        botonEditar = new JButton("EDITAR PROVEEDOR");
        botonEditar.setEnabled(false);
        botonEditar.addActionListener( e -> venEditarProveedor());
        botonesSec.add(botonEditar);
        
        botonAgregar = new JButton("AGREGAR PROVEEDOR");
        botonAgregar.addActionListener( e -> venAgregarProveedor());
        botonesSec.add(botonAgregar);
       
        
        botonesTer = new JPanel();
        botonesTer.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 0));
        
        
        String[] tablas = {"Proveedores","Productos","Clientes"};
        select = new JComboBox(tablas);
        
        JButton itemCon = new JButton("BUSCAR");
        itemCon.addActionListener( e -> busca((String)select.getSelectedItem()));
        botonesTer.add(itemCon);
        botonesTer.add(select);
        
        
        
        esquema = new JPanel();
        esquema.setLayout(new BorderLayout(5,5));
        
        String[] caracteristicas ={"Id","Nombre"};
        
        String[][] datos= new String[proveedores.size()][2];
        
        for(int i=0;i<proveedores.size();i++)
        {
            datos[i][0]=""+proveedores.get(i).getId();
            datos[i][1]=proveedores.get(i).getNombre();
        }
        
        JTable tablaProveedores = new JTable(datos,caracteristicas);
        
        esquema.add(botonesTer, BorderLayout.NORTH);
        //esquema.add(new JLabel(),BorderLayout.SOUTH);
        esquema.add(new JLabel(),BorderLayout.EAST);
        esquema.add(new JLabel(),BorderLayout.WEST);
        esquema.add(new JScrollPane(tablaProveedores),BorderLayout.CENTER);
        
        
        esquema.setBackground(Color.WHITE);
        botones.setBackground(Color.WHITE);
        botonesSec.setBackground(Color.WHITE);
        botonesTer.setBackground(Color.WHITE);
        contenedor.setBackground(Color.WHITE);
        
        
        contenedor.add(botones,BorderLayout.NORTH);
        contenedor.add(esquema,BorderLayout.CENTER);
        contenedor.add(botonesSec,BorderLayout.SOUTH);
        
        ventanaIni.validate();
        ventanaIni.setVisible(true);
	}

	public void venAgregarFondos() {
		ventanaAgregar = new VentanaModal(ventanaIni,true);
        Container contAgre = ventanaAgregar.getContentPane();
        
        ventanaAgregar.setTitle("Agregar Fondos");
        ventanaAgregar.setSize(300,120);
        ventanaAgregar.setResizable(false);
        ventanaAgregar.setLocationRelativeTo(ventanaIni);
        
        JPanel esquema2 = new JPanel(new BorderLayout(20,10));
		JPanel sub = new JPanel(new GridLayout(1,2,5,5));
	
		esquema2.add(new JLabel(),BorderLayout.NORTH);
		esquema2.add(new JLabel(),BorderLayout.SOUTH);
		esquema2.add(new JLabel(),BorderLayout.EAST);
		esquema2.add(new JLabel(),BorderLayout.WEST);
		esquema2.add(sub,BorderLayout.CENTER);
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
        JLabel cap = new JLabel();
        cap .setText("Fondo: ");
        sub.add(cap);
        
        JTextField txtCap = new JTextField();
        txtCap.setText("Ingrese_Capital");
        sub.add(txtCap);
        txtCap.selectAll();
        
        
        JButton btnAProv= new JButton("AGREGAR FONDOS");
        btnAProv.addActionListener( e -> validarAgregarFondo(txtCap.getText().trim()));
        botonesSec2.add(btnAProv);
        
        sub.setBackground(Color.WHITE);
		esquema2.setBackground(Color.WHITE);
        botonesSec2.setBackground(Color.WHITE);
        contAgre.setBackground(Color.WHITE);
        
        contAgre.add(esquema2,BorderLayout.CENTER);
        contAgre.add(botonesSec2,BorderLayout.SOUTH);
		
		
        
        ventanaAgregar.validate();
        ventanaAgregar.setVisible(true);
	}
	
	public void validarAgregarFondo(String ingresoAux) {
		if(ingresoAux.matches("[0-9]*")){
			double ingreso = Double.parseDouble(ingresoAux);
			if (ingreso > 0) {
				try {
					double capitalActual = 0;
					if (capital.size()>0) {
						int indice = capital.size()-1;
						capitalActual = capital.get(indice).getValorActual()+ingreso;
					}else {
						capitalActual = ingreso;
					}
					
					Statement miStatement = miConexion.createStatement();
					String instruccion = "insert into caja (valorIngreso, valorActual) values('"+ingreso+"','"+capitalActual+"')";
					miStatement.executeUpdate(instruccion);
					instruccion = "select * from caja";
					ResultSet miResulset = miStatement.executeQuery(instruccion);
					
					miResulset.next();
					int id = miResulset.getInt("idCaja");
					while(miResulset.next()) {
						if (miResulset.getInt("idCaja")>id) {
							id = miResulset.getInt("idCaja");
						}
					}
					ultima = new Caja(id,ingreso,capitalActual);
					capital.add(ultima);
					System.out.println("id: "+id+"   ingreso:"+ingreso+"   actual:"+capitalActual);
					JOptionPane.showMessageDialog(ventanaAgregar, "Se agregaron fondos exitosamente");
					
				}catch (Exception e) {
					System.out.println("no conecta");
					e.printStackTrace();
				}
				ventanaAgregar.dispose();
				ventanaIni.revalidate();
				ventanaIni.repaint();
				
			}else {
				JOptionPane.showMessageDialog(ventanaAgregar, "El valor del ingreso es negativo");
			}
		}else{
			JOptionPane.showMessageDialog(ventanaAgregar, "No es un valor numerico");
		}
	}
	
	public void venFondos() {
		venFondo = new JFrame();
        Container contAgre = venFondo.getContentPane();
        
        venFondo.setTitle("Ver Fondo");
        venFondo.setSize(300,150);
        venFondo.setResizable(false);
        venFondo.setLocationRelativeTo(ventanaIni);
		
		JPanel esquema2 = new JPanel();
        esquema2.setLayout(new BorderLayout(10,10));
        
		String[] caracteristicas ={"idCaja","ValorIngreso","ValorActual"};
        
        String[][] datos= new String[capital.size()][3];
        
        for(int i=0;i<capital.size();i++)
        {
            datos[i][0]=""+capital.get(i).getId();
            datos[i][1]=""+capital.get(i).getValorIngreso();
            datos[i][2]=""+capital.get(i).getValorActual();
        }
        
        JTable tablaFondos = new JTable(datos,caracteristicas);
        
        esquema2.add(new JLabel(),BorderLayout.SOUTH);
        esquema2.add(new JLabel(),BorderLayout.NORTH);
        esquema2.add(new JLabel(),BorderLayout.EAST);
        esquema2.add(new JLabel(),BorderLayout.WEST);
        esquema2.add(new JScrollPane(tablaFondos),BorderLayout.CENTER);
		
        contAgre.add(esquema2,BorderLayout.CENTER);
		
        
        venFondo.validate();
        venFondo.setVisible(true);
	}
	
	public void venAgregarProducto(){
		
        ventanaAgregar = new VentanaModal(ventanaIni,true);
        Container contAgre = ventanaAgregar.getContentPane();
        
        ventanaAgregar.setTitle("Agregar un Producto");
        ventanaAgregar.setSize(350,200);
        ventanaAgregar.setResizable(false);
        ventanaAgregar.setLocationRelativeTo(ventanaIni);
        
        
        JPanel esquema2 = new JPanel(new BorderLayout(20,5));
		JPanel sub = new JPanel(new GridLayout(3,2,20,20));

		esquema2.add(new JLabel(),BorderLayout.NORTH);
		esquema2.add(new JLabel(),BorderLayout.SOUTH);
		esquema2.add(new JLabel(),BorderLayout.EAST);
		esquema2.add(new JLabel(),BorderLayout.WEST);
		esquema2.add(sub,BorderLayout.CENTER);
		
        
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
        
        
        JLabel nom = new JLabel();
        nom.setText("Nombre: ");
        sub.add(nom);
        
        JTextField txtNom = new JTextField();
        txtNom.setText("Ingresar_Nombre");
        sub.add(txtNom);
        txtNom.selectAll();

        
        
        JLabel idProv = new JLabel();
        idProv.setText("ID Proveedor: ");
        sub.add(idProv);
        
        JComboBox selIdProv = new JComboBox(); 
        try {
			Statement miStatement = miConexion.createStatement();
			String instruccion = "select * from proveedor";
			ResultSet miResulset = miStatement.executeQuery(instruccion);
			
			while(miResulset.next()) {
				selIdProv.addItem(miResulset.getString("nombre"));
			}
			
		}catch (Exception e) {
			System.out.println("no conecta");
			e.printStackTrace();
		}
        
        sub.add(selIdProv);
        
        
        JLabel preCompra= new JLabel();
        preCompra.setText("Precio de Compra: ");
        sub.add(preCompra);
        
        JTextField txtPreCompra = new JTextField();
        txtPreCompra.setText("Precio");
        sub.add(txtPreCompra);
        txtPreCompra.selectAll();
        
        
        JButton btnAProd = new JButton("AGREGAR PRODUCTO");
        btnAProd .addActionListener( e -> validarAgregarProducto(txtNom.getText().trim(),(String)selIdProv.getSelectedItem(),txtPreCompra.getText().trim()));
        botonesSec2.add(btnAProd );
        
        
        sub.setBackground(Color.WHITE);
		esquema2.setBackground(Color.WHITE);
        botonesSec2.setBackground(Color.WHITE);
        contAgre.setBackground(Color.WHITE);
        
        contAgre.add(esquema2,BorderLayout.CENTER);
        contAgre.add(botonesSec2,BorderLayout.SOUTH);
		
        
        ventanaAgregar.validate();
        ventanaAgregar.setVisible(true);
    }
	
	public void validarAgregarProducto(String nombre,String proveedor,String precioCompra) {
		if (nombre.length()<=50) {
			if(precioCompra != "" && precioCompra != null && !precioCompra.isEmpty()) {
				if(precioCompra.matches("[0-9]*")) {
					double precioCompraAux = Double.parseDouble(precioCompra);
					if( nombre != "" && nombre != null && !nombre.isEmpty() && precioCompraAux>0 &&  !Double.isNaN(precioCompraAux))
			        {	
						try {
							Statement miStatement = miConexion.createStatement();
							
							Proveedor provAux = null;
							for (Proveedor i : proveedores) {
								if (proveedor.equalsIgnoreCase(i.getNombre())) {
									provAux = i;
								}
							}
							int id = provAux.getId();
							String instruccion = "insert into producto (idProveedor,nombre,precioCompra,precioVenta,cantidad) values("+id+",'"+nombre+"',"+precioCompra+","+(precioCompraAux*1.3)+","+0+")";
							miStatement.executeUpdate(instruccion);
							instruccion = "select * from producto";
							ResultSet miResulset = miStatement.executeQuery(instruccion);
							
							miResulset.next();
							id = miResulset.getInt("idProducto");
							while(miResulset.next()) {
								if (miResulset.getInt("idProducto")>id) {
									id = miResulset.getInt("idProducto");
								}
							}
							productos.add(new Producto(id,nombre,provAux,precioCompraAux,(precioCompraAux*1.3)));
							System.out.println("id: "+id+"   nombre:"+nombre+"   idProveedor:"+provAux.getId()+"   precioCompra:"+precioCompraAux+"   precioVenta:"+(precioCompraAux*1.3)+"   Cantidad:"+0);
							JOptionPane.showMessageDialog(ventanaAgregar, "Se agrego producto exitosamente");
				
							
						}catch (Exception e) {
							System.out.println("No conecta");
							e.printStackTrace();
						}
						ventanaAgregar.dispose();
						busca("Productos");
			        }else{
			        	JOptionPane.showMessageDialog(ventanaAgregar, "Espacios sin llenar");
			        }
				}else {
					JOptionPane.showMessageDialog(ventanaAgregar, "El precio no es un valor númerico");
				}
			}else {
				JOptionPane.showMessageDialog(ventanaAgregar, "Espacios sin llenar");
			}
		}else {
			JOptionPane.showMessageDialog(ventanaAgregar, "El nombre que ingreso supera los 50 caracteres");
		}
	}
	
	public void venAgregarProveedor(){
		
		ventanaAgregar = new VentanaModal(ventanaIni,true);
		Container contAgre = ventanaAgregar.getContentPane();
        
        ventanaAgregar.setTitle("Agregar un Proveedor");
        ventanaAgregar.setSize(300,120);
        ventanaAgregar.setResizable(false);
        ventanaAgregar.setLocationRelativeTo(ventanaIni);
        
        JPanel esquema2 = new JPanel(new BorderLayout(20,10));
		JPanel sub = new JPanel(new GridLayout(1,2,5,5));
	
		esquema2.add(new JLabel(),BorderLayout.NORTH);
		esquema2.add(new JLabel(),BorderLayout.SOUTH);
		esquema2.add(new JLabel(),BorderLayout.EAST);
		esquema2.add(new JLabel(),BorderLayout.WEST);
		esquema2.add(sub,BorderLayout.CENTER);
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
        JLabel nom = new JLabel();
        nom .setText("Nombre: ");
        sub.add(nom );
        
        JTextField txtNom  = new JTextField();
        txtNom.setText("Nombre_Preveedor");
        sub.add(txtNom);
        txtNom.selectAll();
        
        
        JButton btnAProv= new JButton("AGREGAR PROVEEDOR");
        btnAProv.addActionListener( e -> validarAgregarProveedor(txtNom.getText().trim()));
        botonesSec2.add(btnAProv);
        
        sub.setBackground(Color.WHITE);
		esquema2.setBackground(Color.WHITE);
        botonesSec2.setBackground(Color.WHITE);
        contAgre.setBackground(Color.WHITE);
        
        contAgre.add(esquema2,BorderLayout.CENTER);
        contAgre.add(botonesSec2,BorderLayout.SOUTH);
		
		
        
        ventanaAgregar.validate();
        ventanaAgregar.setVisible(true);
    }	
	
	public void validarAgregarProveedor(String nombre) {
		if (nombre.length()<=50) {
			if(  nombre != "" && nombre != null && !nombre.isEmpty())
	        {
				try {
					Statement miStatement = miConexion.createStatement();
					String instruccion = "select * from proveedor";
					ResultSet miResulset = miStatement.executeQuery(instruccion);
					boolean bol=true;
					while(miResulset.next()) {
						String aux =miResulset.getString("nombre");
						if (aux.equalsIgnoreCase(nombre)) {
							bol = false;
							break;
						}
					}
					
					if(bol) {
						instruccion = "insert into proveedor (nombre) values('"+nombre+"')";
						miStatement.executeUpdate(instruccion);
						instruccion = "select * from proveedor";
						miResulset = miStatement.executeQuery(instruccion);
						
						miResulset.next();
						int id = miResulset.getInt("idProveedor");
						while(miResulset.next()) {
							if (miResulset.getInt("idProveedor")>id) {
								id = miResulset.getInt("idProveedor");
							}
						}
						proveedores.add(new Proveedor(id,nombre));
						System.out.println("id: "+id+"   nombre:"+nombre);
						JOptionPane.showMessageDialog(ventanaAgregar, "Se agrego proveedor exitosamente");
						
						ventanaAgregar.dispose();
						busca("Proveedores");
						
					}else {
						JOptionPane.showMessageDialog(ventanaAgregar, "Ya existe ese Proveedor");
					}
			
					
				}catch (Exception e) {
					System.out.println("No conecta");
					e.printStackTrace();
				}
				
	        }else{
	        	JOptionPane.showMessageDialog(ventanaAgregar, "Espacios sin llenar");
	        }
		}else {
			JOptionPane.showMessageDialog(ventanaAgregar, "El nombre que ingreso supera los 50 caracteres");
		}
	}
	
	
	public void venAgregarCliente()
    {
		ventanaAgregar = new VentanaModal(ventanaIni,true);
		Container contAgre = ventanaAgregar.getContentPane();
        
        ventanaAgregar.setTitle("Agregar un Cliente");
        ventanaAgregar.setSize(300,120);
        ventanaAgregar.setResizable(false);
        ventanaAgregar.setLocationRelativeTo(ventanaIni);
        
        JPanel esquema2 = new JPanel(new BorderLayout(20,10));
		JPanel sub = new JPanel(new GridLayout(1,2,5,5));
	
		esquema2.add(new JLabel(),BorderLayout.NORTH);
		esquema2.add(new JLabel(),BorderLayout.SOUTH);
		esquema2.add(new JLabel(),BorderLayout.EAST);
		esquema2.add(new JLabel(),BorderLayout.WEST);
		esquema2.add(sub,BorderLayout.CENTER);
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
        JLabel nom = new JLabel();
        nom .setText("Nombre: ");
        sub.add(nom );
        
        JTextField txtNom  = new JTextField();
        txtNom.setText("Nombre_Cliente");
        sub.add(txtNom);
        txtNom.selectAll();
        
        
        JButton btnACli= new JButton("AGREGAR CLIENTE");
        btnACli.addActionListener( e -> validarAgregarCliente(txtNom.getText().trim()));
        botonesSec2.add(btnACli);
        
        sub.setBackground(Color.WHITE);
		esquema2.setBackground(Color.WHITE);
        botonesSec2.setBackground(Color.WHITE);
        contAgre.setBackground(Color.WHITE);
        
        contAgre.add(esquema2,BorderLayout.CENTER);
        contAgre.add(botonesSec2,BorderLayout.SOUTH);
		
		
        
        ventanaAgregar.validate();
        ventanaAgregar.setVisible(true);
    }	
	
	public void validarAgregarCliente(String nombre) {
		if (nombre.length()<=50) {
			if(  nombre != "" && nombre != null && !nombre.isEmpty())
	        {
				try {
					Statement miStatement = miConexion.createStatement();
					String instruccion = "select * from cliente";
					ResultSet miResulset = miStatement.executeQuery(instruccion);
					boolean bol=true;
					while(miResulset.next()) {
						String aux =miResulset.getString("nombre");
						if (aux.equalsIgnoreCase(nombre)) {
							bol =false;
						}
					}
					
					if(bol) {
						instruccion = "insert into cliente (nombre) values('"+nombre+"')";
						miStatement.executeUpdate(instruccion);
						instruccion = "select * from cliente";
						miResulset = miStatement.executeQuery(instruccion);
						
						miResulset.next();
						int id = miResulset.getInt("idCliente");
						while(miResulset.next()) {
							if (miResulset.getInt("idCliente")>id) {
								id = miResulset.getInt("idCliente");
							}
						}
						clientes.add(new Cliente(id,nombre));
						System.out.println("id: "+id+"   nombre:"+nombre);
						JOptionPane.showMessageDialog(ventanaAgregar, "Se agrego cliente exitosamente");
						
						ventanaAgregar.dispose();
						busca("Clientes");
					}else {
						JOptionPane.showMessageDialog(ventanaAgregar, "Ya existe ese cliente");
					}
			
					
				}catch (Exception e) {
					System.out.println("No conecta");
					e.printStackTrace();
				}
				
	        }else{
	        	JOptionPane.showMessageDialog(ventanaAgregar, "Espacios sin llenar");
	        }
		}else {
			JOptionPane.showMessageDialog(ventanaAgregar, "El nombre que ingreso supera los 50 caracteres");
		}
	}
	
	
	
	public void venEditarProveedor(){
		
    }
	
	public void venEditarProducto(){
		
    }
	
	public void venEditarCliente(){
		
    }
	
	
	public void ventanaConsulta() {
		
		contenedor.removeAll();
		botonCon.setEnabled(false);
		botonCom.setEnabled(true);
		botonVen.setEnabled(true);
		
		contenedor.add(botones,BorderLayout.NORTH);
        contenedor.add(esquema,BorderLayout.CENTER);
        contenedor.add(botonesSec,BorderLayout.SOUTH);
		
		contenedor.revalidate();
		contenedor.repaint();
		busca((String)select.getSelectedItem());
	}
	
	public void ventanaVenta() {
		
		contenedor.removeAll();
		botonCon.setEnabled(true);
		botonCom.setEnabled(true);
		botonVen.setEnabled(false);
		ventanaIni.setSize(550,300);
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
		JButton botonEditar2 = new JButton("EDITAR VENTA");
        botonEditar2.setEnabled(false);
        botonEditar2.addActionListener( e -> venEditarVenta());
        botonesSec2.add(botonEditar2);
        
        JButton botonAgregar2 = new JButton("REGISTRAR VENTA");
        botonAgregar2.addActionListener( e -> venRegistrarVenta());
        botonesSec2.add(botonAgregar2);
		
        boolean bool = false;
        for (Producto i : productos) {
			if (i.getCantidad()>0) {
				bool = true;
			}
		}
        
        if((productos.size()>0) && bool) {
        	botonAgregar2.setEnabled(true);
        }else {
        	botonAgregar2.setEnabled(false);
        }
        
        
        JPanel esquema2 = new JPanel();
        esquema2.setLayout(new BorderLayout(5,5));
        
		String[] caracteristicas ={"IdVenta","IdProducto","Producto","Proveedor","Cantidad","Precio Final","Fecha"};
        
        String[][] datos= new String[ventas.size()][7];
        
        for(int i=0;i<ventas.size();i++)
        {
            datos[i][0]=""+ventas.get(i).getId();
            datos[i][1]=""+ventas.get(i).getProducto().getId();
            datos[i][2]=""+ventas.get(i).getProducto().getNombre();
            datos[i][3]=""+ventas.get(i).getCliente().getNombre();
            datos[i][4]=""+ventas.get(i).getCantidad();
            datos[i][5]=""+ventas.get(i).getPrecioFinal();
            datos[i][6]=""+ventas.get(i).getFecha();
        }
        
        JTable tablaVentas = new JTable(datos,caracteristicas);
        

        esquema2.add(new JLabel(),BorderLayout.EAST);
        esquema2.add(new JLabel(),BorderLayout.WEST);
        esquema2.add(new JScrollPane(tablaVentas),BorderLayout.CENTER);
		
		
		
		contenedor.add(botones,BorderLayout.NORTH);
        contenedor.add(esquema2,BorderLayout.CENTER);
        contenedor.add(botonesSec2,BorderLayout.SOUTH);
        
		contenedor.revalidate();
		contenedor.repaint();
	}
	
	public void venRegistrarVenta() {
		
		ventanaAgregar = new VentanaModal(ventanaIni,true);
		Container contAgre = ventanaAgregar.getContentPane();
		
		ventanaAgregar.setTitle("Registrar Venta");
        ventanaAgregar.setSize(320,240);
        ventanaAgregar.setResizable(false);
        ventanaAgregar.setLocationRelativeTo(ventanaIni);
        
		
		JPanel esquema2 = new JPanel(new BorderLayout(20,5));
		JPanel sub = new JPanel(new GridLayout(4,1,20,20));
		JPanel sub1 = new JPanel(new GridLayout(1,2,10,10));
		JPanel sub2 = new JPanel(new GridLayout(1,2,10,10));
		JPanel sub3 = new JPanel(new GridLayout(1,6,10,10));
		JPanel sub4 = new JPanel(new GridLayout(1,2,10,10));
		sub.add(sub1);
		sub.add(sub2);
		sub.add(sub3);
		sub.add(sub4);
		esquema2.add(new JLabel(),BorderLayout.NORTH);
		esquema2.add(new JLabel(),BorderLayout.SOUTH);
		esquema2.add(new JLabel(),BorderLayout.EAST);
		esquema2.add(new JLabel(),BorderLayout.WEST);
		esquema2.add(sub,BorderLayout.CENTER);
		
		
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
        
        
        JLabel idProd = new JLabel();
        idProd.setText("ID Producto: ");
        sub1.add(idProd);
        
        JComboBox selIdProd = new JComboBox(); 
        try {
			Statement miStatement = miConexion.createStatement();
			String instruccion = "select * from producto";
			ResultSet miResulset = miStatement.executeQuery(instruccion);
			
			while(miResulset.next()) {
				if(miResulset.getInt("cantidad")>0) {
					selIdProd.addItem(miResulset.getInt("idProducto"));
				}
			}
			
		}catch (Exception e) {
			System.out.println("No conecta");
			e.printStackTrace();
		}
        
        sub1.add(selIdProd);
        
        
        JLabel idCli = new JLabel();
        idCli.setText("Cliente: ");
        sub2.add(idCli);
        
        JComboBox selIdCli = new JComboBox(); 
        try {
			Statement miStatement = miConexion.createStatement();
			String instruccion = "select * from cliente";
			ResultSet miResulset = miStatement.executeQuery(instruccion);
			
			while(miResulset.next()) {
				selIdCli.addItem(miResulset.getString("nombre"));
			}
			
		}catch (Exception e) {
			System.out.println("No conecta");
			e.printStackTrace();
		}
        
        sub2.add(selIdCli);
        
        
        JLabel dia= new JLabel();
        dia.setText("Dia: ");
        sub3.add(dia);
        
        JTextField txtDia = new JTextField();
        txtDia.setText("DD");
        sub3.add(txtDia);
        txtDia.selectAll();
        
        
        JLabel mes= new JLabel();
        mes.setText("Mes: ");
        sub3.add(mes);
        
        JTextField txtMes = new JTextField();
        txtMes.setText("MM");
        sub3.add(txtMes);
        txtMes.selectAll();
        
        
        JLabel anno = new JLabel();
        anno.setText("Año: ");
        sub3.add(anno);
        
        JTextField txtAnno = new JTextField();
        txtAnno.setText("YYYY");
        sub3.add(txtAnno);
        txtAnno.selectAll();
        
        
        JLabel can = new JLabel();
        can.setText("Cantidad: ");
        sub4.add(can);
        
        JTextField txtCan = new JTextField();
        txtCan.setText("Ingese_Cantidad");
        sub4.add(txtCan);
        txtCan.selectAll();
        
        
        JButton btnRVenta = new JButton("REALIZAR VENTA");
        btnRVenta.addActionListener( e -> validarVenta((int)selIdProd.getSelectedItem(),(String)selIdCli.getSelectedItem(),txtDia.getText().trim(),txtMes.getText().trim(),txtAnno.getText().trim(),txtCan.getText().trim()));
        botonesSec2.add(btnRVenta);
        
        sub.setBackground(Color.WHITE);
        sub1.setBackground(Color.WHITE);
        sub2.setBackground(Color.WHITE);
        sub3.setBackground(Color.WHITE);
        sub4.setBackground(Color.WHITE);
		esquema2.setBackground(Color.WHITE);
        botonesSec2.setBackground(Color.WHITE);
        contAgre.setBackground(Color.WHITE);
        
        
        contAgre.add(esquema2,BorderLayout.CENTER);
        contAgre.add(botonesSec2,BorderLayout.SOUTH);
        
        ventanaAgregar.validate();
        ventanaAgregar.setVisible(true);
	}
	
	public void validarVenta(int idProducto,String cliente,String dia,String mes,String anno,String cantidad) {
		if (dia.length()<=2 && mes.length()<=2 && anno.length()<=4) {
			if(dia != "" && dia != null && !dia.isEmpty() && mes != "" && mes != null && !mes.isEmpty() && anno != "" && anno != null && !anno.isEmpty() && cantidad != "" && cantidad != null && !cantidad.isEmpty()) {
				if(dia.matches("[0-9]*") && mes.matches("[0-9]*") && anno.matches("[0-9]*") && cantidad.matches("[0-9]*")) {
					int diaAux = Integer.parseInt(dia);
					int mesAux = Integer.parseInt(mes);
					int annoAux =Integer.parseInt(anno);
					int cantidadAux = Integer.parseInt(cantidad);
					if( diaAux>0  && mesAux>0 && annoAux>0 && cantidadAux>0)
			        {	
						String fecha = String.format("%04d-%02d-%02d", annoAux, mesAux, diaAux);
						Producto prodAux = null;
						for (Producto i : productos) {
							if (i.getId()==idProducto) {
								prodAux = i;
							}
						}
						double precioFinal = prodAux.getPrecioVenta()*cantidadAux;
						
						Cliente cliAux = null;
						for (Cliente i : clientes) {
							if (cliente.equalsIgnoreCase(i.getNombre())) {
								cliAux = i;
							}
						}
						int idCli = cliAux.getId();
						
						if(cantidadAux<=prodAux.getCantidad()) {
							try {
								double capitalActual = ultima.getValorActual()+precioFinal;
								
								Statement miStatement = miConexion.createStatement();
								String  instruccion = "update caja set valorActual="+capitalActual+" where idCaja="+ultima.getId()+"";
								miStatement.executeUpdate(instruccion);
								
								capital.get(capital.size()-1).setValorActual(capitalActual);
								ultima.setValorActual(capitalActual);
								
								int cantidadActual = prodAux.getCantidad()-cantidadAux;
								miStatement = miConexion.createStatement();
								instruccion = "update producto set cantidad="+(cantidadActual)+" where idProducto="+prodAux.getId()+"";
								miStatement.executeUpdate(instruccion);
								
								for (int i=0;i<productos.size();i++) {
									if (productos.get(i).getId()==prodAux.getId()) {
										productos.get(i).setCantidad(cantidadActual);
									}
								}
								
								
								miStatement = miConexion.createStatement();
								instruccion = "insert into registrosventa (idCliente,idProducto,fecha,cantidad) values("+idCli+","+idProducto+",'"+fecha+"',"+cantidadAux+")";
								miStatement.executeUpdate(instruccion);
								instruccion = "select * from  registrosventa";
								ResultSet miResulset = miStatement.executeQuery(instruccion);
								
								miResulset.next();
								int id = miResulset.getInt("idVenta");
								while(miResulset.next()) {
									if (miResulset.getInt("idVenta")>id) {
										id = miResulset.getInt("idVenta");
									}
								}
								
								ventas.add(new Venta(id,prodAux,cliAux,cantidadAux,fecha,precioFinal));
								System.out.println("id: "+id+"   idProdAux:"+prodAux.getId()+"   idProveedor:"+cliAux.getId()+"   cantidad:"+cantidad+"   fecha:"+fecha+"   precioFinal:"+precioFinal);
								
								JOptionPane.showMessageDialog(ventanaAgregar, "Se registro venta exitosamente");
								ventanaAgregar.dispose();
								ventanaVenta();
								
							}catch (Exception e) {
								System.out.println("No conecta");
								e.printStackTrace();
							}
							
						}else {
							JOptionPane.showMessageDialog(ventanaAgregar, "No hay suficiente producto para vender esa cantidad");
						}
							
			        }else{
			        	JOptionPane.showMessageDialog(ventanaAgregar, "Espacios sin llenar");
			        }
				}else {
					JOptionPane.showMessageDialog(ventanaAgregar, "No son valores numericos");
				}
			}else {
				JOptionPane.showMessageDialog(ventanaAgregar, "Espacios sin llenar");
			}
		}else {
			JOptionPane.showMessageDialog(ventanaAgregar, "Fecha invalida");
		}
	}
	
	public void venEditarVenta() {
		
	}
	
	public void validarEdicionVenta() {
		
	}

	public void ventanaCompra() {
		contenedor.removeAll();
		botonCon.setEnabled(true);
		botonCom.setEnabled(false);
		botonVen.setEnabled(true);
		ventanaIni.setSize(550,300);
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
        
		JButton botonEditar2 = new JButton("EDITAR COMPRA");
        botonEditar2.setEnabled(false);
        botonEditar2.addActionListener( e -> venEditarCompra());
        botonesSec2.add(botonEditar2);
        
        JButton botonAgregar2 = new JButton("REGISTRAR COMPRA");
        botonAgregar2.addActionListener( e -> venRegistrarCompra());
        botonesSec2.add(botonAgregar2);
		
        if((productos.size()>0) && capital.size()>0) {
        	botonAgregar2.setEnabled(true);
        }else {
        	botonAgregar2.setEnabled(false);
        }
		
        JPanel esquema2 = new JPanel();
        esquema2.setLayout(new BorderLayout(5,5));
        
		String[] caracteristicas ={"IdCompra","IdProducto","Producto","Proveedor","Cantidad","Precio Final","Fecha"};
        
        String[][] datos= new String[compras.size()][7];
        
        for(int i=0;i<compras.size();i++)
        {
            datos[i][0]=""+compras.get(i).getId();
            datos[i][1]=""+compras.get(i).getProducto().getId();
            datos[i][2]=""+compras.get(i).getProducto().getNombre();
            datos[i][3]=""+compras.get(i).getProveedor().getNombre();
            datos[i][4]=""+compras.get(i).getCantidad();
            datos[i][5]=""+compras.get(i).getPrecioFinal();
            datos[i][6]=""+compras.get(i).getFecha();
        }
        
        JTable tablaCompras = new JTable(datos,caracteristicas);
        
        esquema2.add(new JLabel(),BorderLayout.EAST);
        esquema2.add(new JLabel(),BorderLayout.WEST);
        esquema2.add(new JScrollPane(tablaCompras),BorderLayout.CENTER);
		
		
		
		contenedor.add(botones,BorderLayout.NORTH);
        contenedor.add(esquema2,BorderLayout.CENTER);
        contenedor.add(botonesSec2,BorderLayout.SOUTH);
        
		contenedor.revalidate();
		contenedor.repaint();
	}
	
	public void venRegistrarCompra() {
		ventanaAgregar = new VentanaModal(ventanaIni,true);
		Container contAgre = ventanaAgregar.getContentPane();
		
		ventanaAgregar.setTitle("Registrar Venta");
        ventanaAgregar.setSize(320,200);
        ventanaAgregar.setResizable(false);
        ventanaAgregar.setLocationRelativeTo(ventanaIni);
        
        
        JPanel esquema2 = new JPanel(new BorderLayout(20,5));
		JPanel sub = new JPanel(new GridLayout(3,1,20,20));
		JPanel sub1 = new JPanel(new GridLayout(1,2,10,10));
		JPanel sub2 = new JPanel(new GridLayout(1,6,10,10));
		JPanel sub3 = new JPanel(new GridLayout(1,2,10,10));
		sub.add(sub1);
		sub.add(sub2);
		sub.add(sub3);
		esquema2.add(new JLabel(),BorderLayout.NORTH);
		esquema2.add(new JLabel(),BorderLayout.SOUTH);
		esquema2.add(new JLabel(),BorderLayout.EAST);
		esquema2.add(new JLabel(),BorderLayout.WEST);
		esquema2.add(sub,BorderLayout.CENTER);
		
		
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
        
        
        JLabel idProd = new JLabel();
        idProd.setText("ID Producto: ");
        sub1.add(idProd);
        
        JComboBox selIdProd = new JComboBox(); 
        try {
			Statement miStatement = miConexion.createStatement();
			String instruccion = "select * from producto";
			ResultSet miResulset = miStatement.executeQuery(instruccion);
			
			while(miResulset.next()) {
				selIdProd.addItem(miResulset.getInt("idProducto"));
			}
			
		}catch (Exception e) {
			System.out.println("No conecta");
			e.printStackTrace();
		}
        
        sub1.add(selIdProd);
        
        
        JLabel dia= new JLabel();
        dia.setText("Dia: ");
        sub2.add(dia);
        
        JTextField txtDia = new JTextField();
        txtDia.setText("DD");
        sub2.add(txtDia);
        txtDia.selectAll();
        
        
        JLabel mes= new JLabel();
        mes.setText("Mes: ");
        sub2.add(mes);
        
        JTextField txtMes = new JTextField();
        txtMes.setText("MM");
        sub2.add(txtMes);
        txtMes.selectAll();
        
        
        JLabel anno = new JLabel();
        anno.setText("Año: ");
        sub2.add(anno);
        
        JTextField txtAnno = new JTextField();
        txtAnno.setText("YYYY");
        sub2.add(txtAnno);
        txtAnno.selectAll();
        
        
        JLabel can = new JLabel();
        can.setText("Cantidad: ");
        sub3.add(can);
        
        JTextField txtCan = new JTextField();
        txtCan.setText("Ingese_Cantidad");
        sub3.add(txtCan);
        txtCan.selectAll();
        
        
        JButton btnRCompra = new JButton("REALIZAR COMPRA");
        btnRCompra.addActionListener( e -> validarCompra((int)selIdProd.getSelectedItem(),txtDia.getText().trim(),txtMes.getText().trim(),txtAnno.getText().trim(),txtCan.getText().trim()));
        botonesSec2.add(btnRCompra);
        
        sub.setBackground(Color.WHITE);
        sub1.setBackground(Color.WHITE);
        sub2.setBackground(Color.WHITE);
        sub3.setBackground(Color.WHITE);
		esquema2.setBackground(Color.WHITE);
        botonesSec2.setBackground(Color.WHITE);
        contAgre.setBackground(Color.WHITE);
        
		
		contAgre.add(esquema2,BorderLayout.CENTER);
        contAgre.add(botonesSec2,BorderLayout.SOUTH);
        
        ventanaAgregar.validate();
        ventanaAgregar.setVisible(true);
        
	}
	
	public void validarCompra(int idProducto,String dia,String mes,String anno,String cantidad) {
		if (dia.length()<=2 && mes.length()<=2 && anno.length()==4) {
			if(dia != "" && dia != null && !dia.isEmpty() && mes != "" && mes != null && !mes.isEmpty() && anno != "" && anno != null && !anno.isEmpty() && cantidad != "" && cantidad != null && !cantidad.isEmpty()) {
				if(dia.matches("[0-9]*") && mes.matches("[0-9]*") && anno.matches("[0-9]*") && cantidad.matches("[0-9]*")) {
					int diaAux = Integer.parseInt(dia);
					int mesAux = Integer.parseInt(mes);
					int annoAux =Integer.parseInt(anno);
					int cantidadAux = Integer.parseInt(cantidad);
					if( diaAux>0  && mesAux>0 && annoAux>0 && cantidadAux>0){	
						
						String fecha = String.format("%04d-%02d-%02d", annoAux, mesAux, diaAux);
						Producto prodAux = null;
						for (Producto i : productos) {
							if (i.getId()==idProducto) {
								prodAux = i;
							}
						}
						
						Proveedor provAux = prodAux.getProveedor();
						int idProv = provAux.getId();
						
						double precioFinal = prodAux.getPrecioCompra()*cantidadAux;
						int can = (int) Math.round(cantidadAux);
						
						
						
						if(ultima.getValorActual()>=precioFinal) {
							try {
								
								double capitalActual = ultima.getValorActual()-precioFinal;
								
								Statement miStatement = miConexion.createStatement();
								String  instruccion = "update caja set valorActual="+capitalActual+" where idCaja="+ultima.getId()+"";
								miStatement.executeUpdate(instruccion);
								
								capital.get(capital.size()-1).setValorActual(capitalActual);
								ultima.setValorActual(capitalActual);
								
								int cantidadActual = cantidadAux+prodAux.getCantidad();
								miStatement = miConexion.createStatement();
								instruccion = "update producto set cantidad="+(cantidadActual)+" where idProducto="+prodAux.getId()+"";
								miStatement.executeUpdate(instruccion);
								
								for (int i=0;i<productos.size();i++) {
									if (productos.get(i).getId()==prodAux.getId()) {
										productos.get(i).setCantidad(cantidadActual);
									}
								}
								
								miStatement = miConexion.createStatement();
								instruccion = "insert into registroscompra (idProveedor,idProducto,fecha,cantidad) values("+idProv+","+idProducto+",'"+fecha+"',"+can+")";
								miStatement.executeUpdate(instruccion);
								instruccion = "select * from  registroscompra";
								ResultSet miResulset = miStatement.executeQuery(instruccion);
								
								miResulset.next();
								int id = miResulset.getInt("idCompra");
								while(miResulset.next()) {
									if (miResulset.getInt("idCompra")>id) {
										id = miResulset.getInt("idCompra");
									}
								}
								
								compras.add(new Compra(id,prodAux,provAux,can,fecha,precioFinal));
								System.out.println("id: "+id+"   idProdAux:"+prodAux.getId()+"   idProveedor:"+provAux.getId()+"   cantidad:"+cantidad+"   fecha:"+fecha+"   precioFinal:"+precioFinal);
								JOptionPane.showMessageDialog(ventanaAgregar, "Se registro compra exitosamente");
								ventanaAgregar.dispose();
								ventanaCompra();
									
								
							}catch (Exception e) {
								System.out.println("No conecta");
								e.printStackTrace();
							}
						}else {
							JOptionPane.showMessageDialog(ventanaAgregar, "No hay plata para comprar esa cantidad de productos");
						}

			        }else{
			        	JOptionPane.showMessageDialog(ventanaAgregar, "Espacios sin llenar");
			        }
				}else {
					JOptionPane.showMessageDialog(ventanaAgregar, "No son valores numericos");
				}
			}else {
				JOptionPane.showMessageDialog(ventanaAgregar, "Espacios sin llenar");
			}
		}else {
			JOptionPane.showMessageDialog(ventanaAgregar, "Fecha invalida");
		}
	}
	
	public void venEditarCompra() {
		
	}
	
	public void validarEdicionCompra() {
		
	}
	
	public void busca(String opcion) {
		
		if (opcion == "Proveedores") {
			
			contenedor.removeAll();;
			esquema.removeAll();
			botonesSec.removeAll();
			ventanaIni.setSize(400,300);
			
			
			botonEditar = new JButton("EDITAR PROVEEDOR");
	        botonEditar.setEnabled(false);
	        botonEditar.addActionListener( e -> venEditarProveedor());
	        botonesSec.add(botonEditar);
	        
	        botonAgregar = new JButton("AGREGAR PROVEEDOR");
	        botonAgregar.addActionListener( e -> venAgregarProveedor());
	        botonesSec.add(botonAgregar);
			
			
			
			String[] caracteristicas ={"Id","Nombre"};
	        
	        String[][] datos= new String[proveedores.size()][2];
	        
	        for(int i=0;i<proveedores.size();i++)
	        {
	            datos[i][0]=""+proveedores.get(i).getId();
	            datos[i][1]=proveedores.get(i).getNombre();
	        }
	        
	        JTable tablaProveedores = new JTable(datos,caracteristicas);
	        
	        esquema.add(botonesTer, BorderLayout.NORTH);
	        //esquema.add(new JLabel(),BorderLayout.SOUTH);
	        esquema.add(new JLabel(),BorderLayout.EAST);
	        esquema.add(new JLabel(),BorderLayout.WEST);
	        esquema.add(new JScrollPane(tablaProveedores),BorderLayout.CENTER);
	        
	        
			
			contenedor.add(botones,BorderLayout.NORTH);
	        contenedor.add(esquema,BorderLayout.CENTER);
	        contenedor.add(botonesSec,BorderLayout.SOUTH);
			
			contenedor.revalidate();
			contenedor.repaint();
			
		}else if(opcion == "Productos") {
			
			contenedor.removeAll();
			esquema.removeAll();
			botonesSec.removeAll();
			ventanaIni.setSize(550,300);
			 
			JButton botonEditar2 = new JButton("EDITAR PRODUCTOS");
	        botonEditar2.setEnabled(false);
	        botonEditar2.addActionListener( e -> venEditarProducto());
	        botonesSec.add(botonEditar2);
	        
	        JButton botonAgregar2 = new JButton("AGREGAR PRODUCTOS");
	        botonAgregar2.addActionListener( e -> venAgregarProducto());
	        botonesSec.add(botonAgregar2);
			
	        if(proveedores.size()>0) {
	        	botonAgregar2.setEnabled(true);
	        }else {
	        	botonAgregar2.setEnabled(false);
	        }
			
			String[] caracteristicas ={"Id","Nombre","Cantidad","PrecioCompra","PrecioVenta","Proveedor"};
	        
	        String[][] datos= new String[productos.size()][6];
	        
	        for(int i=0;i<productos.size();i++)
	        {
	            datos[i][0]=""+productos.get(i).getId();
	            datos[i][1]=productos.get(i).getNombre();
	            datos[i][2]=""+productos.get(i).getCantidad();
	            datos[i][3]=""+productos.get(i).getPrecioCompra();
	            datos[i][4]=""+productos.get(i).getPrecioVenta();
	            datos[i][5]=""+productos.get(i).getProveedor().getNombre();
	        }
	        
	        JTable tablaProductos = new JTable(datos,caracteristicas);
	        
	        esquema.add(botonesTer, BorderLayout.NORTH);
	        //esquema.add(new JLabel(),BorderLayout.SOUTH);
	        esquema.add(new JLabel(),BorderLayout.EAST);
	        esquema.add(new JLabel(),BorderLayout.WEST);
	        esquema.add(new JScrollPane(tablaProductos),BorderLayout.CENTER);
			
			
			contenedor.add(botones,BorderLayout.NORTH);
	        contenedor.add(esquema,BorderLayout.CENTER);
	        contenedor.add(botonesSec,BorderLayout.SOUTH);
			
			contenedor.revalidate();
			contenedor.repaint();
			
		}else {
			
			contenedor.removeAll();
			esquema.removeAll();
			botonesSec.removeAll();
			ventanaIni.setSize(400,300);
			
			
			JButton botonEditar2 = new JButton("EDITAR CLIENTES");
	        botonEditar2.setEnabled(false);
	        botonEditar2.addActionListener( e -> venEditarCliente());
	        botonesSec.add(botonEditar2);
	        
	        JButton botonAgregar2 = new JButton("AGREGAR CLIENTES");
	        botonAgregar2.addActionListener( e -> venAgregarCliente());
	        botonesSec.add(botonAgregar2);
			
			
			
			String[] caracteristicas ={"Id","Nombre"};
	        
	        String[][] datos= new String[clientes.size()][2];
	        
	        for(int i=0;i<clientes.size();i++)
	        {
	            datos[i][0]=""+clientes.get(i).getId();
	            datos[i][1]=clientes.get(i).getNombre();
	        }
	        
	        JTable tablaClientes = new JTable(datos,caracteristicas);
	        
	        esquema.add(botonesTer, BorderLayout.NORTH);
	        //esquema.add(new JLabel(),BorderLayout.SOUTH);
	        esquema.add(new JLabel(),BorderLayout.EAST);
	        esquema.add(new JLabel(),BorderLayout.WEST);
	        esquema.add(new JScrollPane(tablaClientes),BorderLayout.CENTER);
			
			
			contenedor.add(botones,BorderLayout.NORTH);
	        contenedor.add(esquema,BorderLayout.CENTER);
	        contenedor.add(botonesSec,BorderLayout.SOUTH);
			
			contenedor.revalidate();
			contenedor.repaint();
			
		}
		
	}
	
	public void venAnularVenta() {
		ventanaAgregar = new VentanaModal(ventanaIni,true);
		Container contAgre = ventanaAgregar.getContentPane();
        
        ventanaAgregar.setTitle("Anular Venta");
        ventanaAgregar.setSize(300,120);
        ventanaAgregar.setResizable(false);
        ventanaAgregar.setLocationRelativeTo(ventanaIni);
        
        JPanel esquema2 = new JPanel(new BorderLayout(20,10));
		JPanel sub = new JPanel(new GridLayout(1,2,5,5));
	
		esquema2.add(new JLabel(),BorderLayout.NORTH);
		esquema2.add(new JLabel(),BorderLayout.SOUTH);
		esquema2.add(new JLabel(),BorderLayout.EAST);
		esquema2.add(new JLabel(),BorderLayout.WEST);
		esquema2.add(sub,BorderLayout.CENTER);
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
        JLabel idVen = new JLabel();
        idVen .setText("IdVenta: ");
        sub.add(idVen );
        
        
        JComboBox selIdVen = new JComboBox(); 
        try {
			Statement miStatement = miConexion.createStatement();
			String instruccion = "select * from registrosventa";
			ResultSet miResulset = miStatement.executeQuery(instruccion);
			
			while(miResulset.next()) {
				selIdVen.addItem(miResulset.getInt("idVenta"));
			}
			
		}catch (Exception e) {
			System.out.println("No conecta");
			e.printStackTrace();
		}
        
        sub.add(selIdVen);
        
        JButton btnAnularVenta= new JButton("ANULAR VENTA");
        btnAnularVenta.addActionListener( e -> validarAnularVenta((int)selIdVen.getSelectedItem()));
        botonesSec2.add(btnAnularVenta);
        
        sub.setBackground(Color.WHITE);
		esquema2.setBackground(Color.WHITE);
        botonesSec2.setBackground(Color.WHITE);
        contAgre.setBackground(Color.WHITE);
        
        contAgre.add(esquema2,BorderLayout.CENTER);
        contAgre.add(botonesSec2,BorderLayout.SOUTH);

        
        ventanaAgregar.validate();
        ventanaAgregar.setVisible(true);
        
	}
	
	
	
	
	
	
	public void validarAnularVenta(int idVenta) {
		
		int indV = 0;
		for (int i=0;i<ventas.size();i++) {
			if (ventas.get(i).getId()==idVenta) {
				indV = i;
			}
		}
		int indP = 0;
		for (int i=0;i<productos.size();i++) {
			if (productos.get(i).getId()==ventas.get(indV).getProducto().getId()) {
				indP = i;
			}
		}
		
		if (ventas.get(indV).getPrecioFinal()<=ultima.getValorActual()) {
			double capitalActual = ultima.getValorActual()-ventas.get(indV).getPrecioFinal();
			int cantidadActual = ventas.get(indV).getCantidad()+productos.get(indP).getCantidad();
			
			capital.get(capital.size()-1).setValorActual(capitalActual);
			ultima.setValorActual(capitalActual);
			
			productos.get(indP).setCantidad(cantidadActual);
			
			ventas.remove(indV);
			
			try {
				Statement miStatement = miConexion.createStatement();
				String  instruccion = "update caja set valorActual="+capitalActual+" where idCaja="+ultima.getId()+"";
				miStatement.executeUpdate(instruccion);
				
				miStatement = miConexion.createStatement();
				instruccion = "update producto set cantidad="+(cantidadActual)+" where idProducto="+productos.get(indP).getId()+"";
				miStatement.executeUpdate(instruccion);
				
				
				miStatement = miConexion.createStatement();
				instruccion = "delete from registrosventa where idVenta="+idVenta+"";
				miStatement.executeUpdate(instruccion);
				
				JOptionPane.showMessageDialog(ventanaAgregar, "Se anulo venta exitosamente");
				ventanaAgregar.dispose();
				ventanaVenta();
				
			}catch (Exception e) {
				System.out.println("No conecta");
				e.printStackTrace();
			}

		}else {
			JOptionPane.showMessageDialog(ventanaAgregar, "No hay dinero para regresarle al cliente");
		}
	}
	
	public void venAnularCompra() {
		ventanaAgregar = new VentanaModal(ventanaIni,true);
		Container contAgre = ventanaAgregar.getContentPane();
        
        ventanaAgregar.setTitle("Anular Compra");
        ventanaAgregar.setSize(300,120);
        ventanaAgregar.setResizable(false);
        ventanaAgregar.setLocationRelativeTo(ventanaIni);
        
        JPanel esquema2 = new JPanel(new BorderLayout(20,10));
		JPanel sub = new JPanel(new GridLayout(1,2,5,5));
	
		esquema2.add(new JLabel(),BorderLayout.NORTH);
		esquema2.add(new JLabel(),BorderLayout.SOUTH);
		esquema2.add(new JLabel(),BorderLayout.EAST);
		esquema2.add(new JLabel(),BorderLayout.WEST);
		esquema2.add(sub,BorderLayout.CENTER);
		
		JPanel botonesSec2 = new JPanel();
        botonesSec2.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));
		
        JLabel idCom = new JLabel();
        idCom .setText("idCompra: ");
        sub.add(idCom );
        
        JComboBox selIdCom = new JComboBox(); 
        try {
			Statement miStatement = miConexion.createStatement();
			String instruccion = "select * from registroscompra";
			ResultSet miResulset = miStatement.executeQuery(instruccion);
			
			while(miResulset.next()) {
				selIdCom.addItem(miResulset.getInt("idCompra"));
			}
			
		}catch (Exception e) {
			System.out.println("No conecta");
			e.printStackTrace();
		}
        
        sub.add(selIdCom);
        
        JButton btnAnularCompra= new JButton("ANULAR COMPRA");
        btnAnularCompra.addActionListener( e -> validarAnularCompra((int)selIdCom.getSelectedItem()));
        botonesSec2.add(btnAnularCompra);
        
        sub.setBackground(Color.WHITE);
		esquema2.setBackground(Color.WHITE);
        botonesSec2.setBackground(Color.WHITE);
        contAgre.setBackground(Color.WHITE);
        
        contAgre.add(esquema2,BorderLayout.CENTER);
        contAgre.add(botonesSec2,BorderLayout.SOUTH);
        
        ventanaAgregar.validate();
        ventanaAgregar.setVisible(true);
	}
	
	public void validarAnularCompra(int idCompra) {
		int indC = 0;
		for (int i=0;i<compras.size();i++) {
			if (compras.get(i).getId()==idCompra) {
				indC = i;
			}
		}
		int indP = 0;
		for (int i=0;i<productos.size();i++) {
			if (productos.get(i).getId()==compras.get(indC).getProducto().getId()) {
				indP = i;
			}
		}
		
		if (compras.get(indC).getCantidad()<=productos.get(indP).getCantidad()) {
			double capitalActual = ultima.getValorActual()+compras.get(indC).getPrecioFinal();
			int cantidadActual = productos.get(indP).getCantidad()-compras.get(indC).getCantidad();
			
			capital.get(capital.size()-1).setValorActual(capitalActual);
			ultima.setValorActual(capitalActual);
			
			productos.get(indP).setCantidad(cantidadActual);
			
			compras.remove(indC);
			
			try {
				Statement miStatement = miConexion.createStatement();
				String  instruccion = "update caja set valorActual="+capitalActual+" where idCaja="+ultima.getId()+"";
				miStatement.executeUpdate(instruccion);
				
				miStatement = miConexion.createStatement();
				instruccion = "update producto set cantidad="+(cantidadActual)+" where idProducto="+productos.get(indP).getId()+"";
				miStatement.executeUpdate(instruccion);
				
				
				miStatement = miConexion.createStatement();
				instruccion = "delete from registroscompra where idCompra="+idCompra+"";
				miStatement.executeUpdate(instruccion);
				
				JOptionPane.showMessageDialog(ventanaAgregar, "Se anulo compra exitosamente");
				ventanaAgregar.dispose();
				ventanaCompra();
				
			}catch (Exception e) {
				System.out.println("No conecta");
				e.printStackTrace();
			}

		}else {
			JOptionPane.showMessageDialog(ventanaAgregar, "No hay dinero para regresarle al cliente");
		}
	}
	
	public ArrayList<Producto> getProductos() {
		return productos;
	}

	public void setProductos(ArrayList<Producto> productos) {
		this.productos = productos;

	}

	public ArrayList<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(ArrayList<Cliente> clientes) {
		this.clientes = clientes;

	}

	public ArrayList<Proveedor> getProveedores() {
		return proveedores;
	}

	public void setProveedores(ArrayList<Proveedor> proveedores) {
		this.proveedores = proveedores;

	}

	public ArrayList<Venta> getVentas() {
		return ventas;
	}

	public void setVentas(ArrayList<Venta> ventas) {
		this.ventas = ventas;

	}

	public ArrayList<Compra> getCompras() {
		return compras;
	}

	public void setCompras(ArrayList<Compra> compras) {
		this.compras = compras;

	}
	public ArrayList<Caja> getCapital() {
		return capital;
	}

	public void setCapital(ArrayList<Caja> capital) {
		this.capital = capital;

	}
	public Caja getUltima() {
		return ultima;
	}

	public void setUltima(Caja ultima) {
		this.ultima = ultima;
	}
}


