package Inventario;
import java.sql.*;
import java.util.*;

public class Principal {
	
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<Producto> productos = new ArrayList<Producto>();
	    ArrayList<Cliente> clientes = new ArrayList<Cliente>();
	    ArrayList<Proveedor> proveedores = new ArrayList<Proveedor>();
	    ArrayList<Venta> ventas = new ArrayList<Venta>();
	    ArrayList<Compra> compras = new ArrayList<Compra>();
	    ArrayList<Caja> capital = new ArrayList<Caja>();
	    int id;
	    
		try {
			
			Connection miConexion=DriverManager.getConnection("jdbc:mysql://localhost:3306/inventario", "root", "");
			
			Statement miStatement = miConexion.createStatement();
			
			ResultSet miResulset = miStatement.executeQuery("select * from proveedor");
			
			while(miResulset.next()) {
				proveedores.add(new Proveedor(miResulset.getInt("idProveedor"),miResulset.getString("nombre")));
			}
			

			miResulset = miStatement.executeQuery("select * from cliente");
			
			while(miResulset.next()) {
				clientes.add(new Cliente(miResulset.getInt("idCliente"),miResulset.getString("nombre")));
			}
			

			miResulset = miStatement.executeQuery("select * from producto");
			
			Proveedor provAux = null;
			while(miResulset.next()) {
				id = miResulset.getInt("idProveedor");
				for (Proveedor i : proveedores) {
					if (i.getId() == id) {
						provAux = i;
					}
				}
				productos.add(new Producto(miResulset.getInt("idProducto"),miResulset.getString("nombre"), provAux, miResulset.getDouble("precioCompra"),miResulset.getDouble("precioVenta"),miResulset.getInt("cantidad")));
			}
			
			
			miResulset = miStatement.executeQuery("select * from registrosventa");
			
			Producto prodAux = null;
			Cliente cliAux = null;
			while(miResulset.next()) {
				id = miResulset.getInt("idProducto");
				for (Producto i : productos) {
					if (i.getId() == id) {
						prodAux = i;
					}
				}
				id = miResulset.getInt("idCliente");
				for (Cliente i : clientes) {
					if (i.getId() == id) {
						cliAux = i;
					}
				}
				ventas.add(new Venta(miResulset.getInt("idVenta"), prodAux, cliAux, miResulset.getInt("cantidad"), miResulset.getString("fecha"), (prodAux.getPrecioVenta()*miResulset.getInt("cantidad"))));
			}
			
			
			miResulset = miStatement.executeQuery("select * from registroscompra");
			
			prodAux = null;
			provAux = null;
			while(miResulset.next()) {
				id = miResulset.getInt("idProducto");
				for (Producto i : productos) {
					if (i.getId() == id) {
						prodAux = i;
					}
				}
				id = miResulset.getInt("idProveedor");
				for (Proveedor i : proveedores) {
					if (i.getId() == id) {
						provAux = i;
					}
				}
				compras.add(new Compra(miResulset.getInt("idCompra"), prodAux, provAux, miResulset.getInt("cantidad"), miResulset.getString("fecha"), (prodAux.getPrecioCompra()*miResulset.getInt("cantidad"))));
			}
			
			
			miResulset = miStatement.executeQuery("select * from caja");
			
			while(miResulset.next()) {
				capital.add(new Caja(miResulset.getInt("idCaja"),miResulset.getDouble("valorIngreso"),miResulset.getDouble("valorActual")));
			}
			
			
		}catch(Exception e) {
			System.out.println("no conecta");
			e.printStackTrace();
		}
		
		Ventana ven = new Ventana();
		ven.setClientes(clientes);
		ven.setProveedores(proveedores);
		ven.setProductos(productos);
		ven.setCompras(compras);
		ven.setVentas(ventas);
		ven.setCapital(capital);
		if(capital.size()>0) {
		ven.setUltima(capital.get(capital.size()-1));
		}
		ven.busca("Proveedores");
	}

}
