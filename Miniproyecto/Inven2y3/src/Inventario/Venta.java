package Inventario;

import java.sql.Date;

public class Venta {
	private int id;
	private Producto producto;
	private Cliente cliente;
	private int cantidad;
	private String fecha;
	private double precioFinal;
	
	public Venta(int id, Producto producto, Cliente cliente, int cantidad, String fecha, double precioFinal) {
		this.id = id;
		this.producto = producto;
		this.cliente = cliente;
		this.cantidad = cantidad;
		this.fecha = fecha;
		this.precioFinal = precioFinal;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public double getPrecioFinal() {
		return precioFinal;
	}

	public void setPrecioFinal(double precioFinal) {
		this.precioFinal = precioFinal;
	}
}
