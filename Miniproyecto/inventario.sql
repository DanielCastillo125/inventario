

CREATE DATABASE inventario

CREATE TABLE caja (
  	idCaja int NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  	valorIngreso double NOT NULL,
  	valorActual double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE cliente (
  idCliente int NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  nombre varchar(50) NOT NULL UNIQUE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE proveedor (
  	idProveedor int NOT NULL AUTO_INCREMENT PRIMARY KEY ,
  	nombre varchar(50) NOT NULL UNIQUE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


CREATE TABLE producto (
	idProducto int NOT NULL AUTO_INCREMENT PRIMARY KEY,
	idProveedor int NOT NULL,
	nombre varchar(50) NOT NULL,
	precioCompra double NOT NULL,
	precioVenta double NOT NULL,
	cantidad int NOT NULL 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE producto ADD CONSTRAINT fk_idprovedor FOREIGN KEY (idProveedor) REFERENCES proveedor(idProveedor) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE registroscompra (
  	idCompra int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  	idProveedor int NOT NULL,
  	idProducto int NOT NULL,
  	fecha varchar(10) NOT NULL,
  	cantidad int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE registroscompra ADD CONSTRAINT fk_idprov FOREIGN KEY (idProveedor) REFERENCES proveedor(idProveedor) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE registroscompra ADD CONSTRAINT fk_idprod FOREIGN KEY (idProducto) REFERENCES producto(idProducto) ON DELETE CASCADE ON UPDATE CASCADE;


CREATE TABLE registrosventa (
  idVenta int NOT NULL AUTO_INCREMENT PRIMARY KEY,
  idCliente int NOT NULL,
  idProducto int NOT NULL,
  fecha varchar(10) NOT NULL,
  cantidad int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

ALTER TABLE registrosventa ADD CONSTRAINT fk_idcliente FOREIGN KEY (idCliente) REFERENCES cliente(idCliente) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE registrosventa ADD CONSTRAINT fk_idProduct FOREIGN KEY (idProducto) REFERENCES producto(idProducto) ON DELETE CASCADE ON UPDATE CASCADE;

